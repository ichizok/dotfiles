"" ------------------
"" Prologue
"" ------------------

if &encoding !=? 'utf-8'
  let &termencoding = &encoding
  set encoding=utf-8
endif

scriptencoding utf-8


"" Vim home directory
let $VIMHOME = expand('~/.vim')


"" Reset vimrc autocmd
augroup vimrc
  autocmd!
augroup END


"" Load config
function! s:eager_source(file) abort
  let path = $VIMHOME . '/eager/' . a:file
  if filereadable(path)
    source `=path`
  endif
endfunction


"" Encoding settings
call s:eager_source('encode.vim')


"" Machine-local settings
call s:eager_source('mlocal.vim')


"" General settings
set backspace=indent,eol,start
set cinoptions=:0,l1,g0,N-s,(1s,*100
set cmdheight=1
set display=lastline
set expandtab
set foldenable
set foldmethod=marker
set history=1000
set hlsearch
set ignorecase
set incsearch
set isfname-==
set laststatus=2
set modeline
set modelines=5
set nowrapscan
set pastetoggle=<F2>
set pumheight=20
set shiftwidth=4
set showcmd
set showmode
set showtabline=2
set smartcase
set smarttab
set tabpagemax=50
set tabstop=8
set tags=./tags;
set timeoutlen=500
set ttimeout
set ttimeoutlen=100
set undodir=~/.cache/vim/undo
set undofile
set viminfo=!,%,'50,<100
set wildmenu

if has('cscope')
  set cscopetag
endif

if has('patch-8.1.0360') && &diffopt =~# 'internal'
  set diffopt+=algorithm:histogram,indent-heuristic
endif

if has('patch-8.1.1270')
  set shortmess-=S
endif

if has('patch-8.2.0860')
  set nrformats+=unsigned
endif

let &grepprg = get(filter([
      \   'rg --vimgrep',
      \   'ag --vimgrep',
      \   'jvgrep -I',
      \   'grep -HInR',
      \ ], 'executable(matchstr(v:val, ''^\S\+''))'), 0, &grepprg)

let g:netrw_list_hide = '^\%(\..*\)\?\.sw.$,^\.\%(git\|hg\|svn\),^\.DS_Store'


"" Keymaps
nnoremap Y y$
nnoremap j gj
nnoremap k gk
nnoremap gj j
nnoremap gk k
nnoremap ZZ <Nop>
nnoremap <Leader>ZZ ZZ
nnoremap ZQ <Nop>
nnoremap <Leader>ZQ ZQ


"" Insert mode cursor
inoremap <C-F> <C-G>U<Right>
inoremap <C-B> <C-G>U<Left>


"" Refresh screen and :nohlsearch
nnoremap <silent> <C-L> :<C-U>nohlsearch<CR><C-L>


"" Edit vimrc
nnoremap <silent> <Leader>ev :<C-U>edit $MYVIMRC<CR>
nnoremap <silent> <Leader>eg :<C-U>edit $MYGVIMRC<CR>


"" Replace word under cursor
nnoremap ,r :<C-U>%s/\<<C-R>=escape(expand('<cword>'), '\\.*$^[]')<CR>\>/
nnoremap ,R yiwgv:s/\<<C-R>=escape(@", '\\.*$^[]')<CR>\>/
xnoremap ,r y:<C-U>%s/<C-R>=escape(@", '\\.*/$^[] ')<CR>/
xnoremap ,R :s/<C-R>=escape(@", '\\.*/$^[] ')<CR>/


"" Tab operation
nnoremap g[ gT
cnoreabbrev <expr> te (getcmdtype() ==# ':' && getcmdline() ==# 'te') ? 'tabe' : 'te'


"" Terminal mode
tnoremap <C-S> <C-W>N


"" Toggle number/relativenumber
nnoremap <silent> <F3> :<C-U>call <SID>toggle_number()<CR>
if exists('+relativenumber')
  function! s:toggle_number() abort
    if &l:relativenumber
      setlocal nonumber norelativenumber
    elseif &l:number
      setlocal relativenumber
    else
      setlocal number
    endif
  endfunction
else
  function! s:toggle_number() abort
    setlocal number!
  endfunction
endif


"" Prepare undodir
if !isdirectory(&undodir)
  try
    call mkdir(&undodir, 'p')
  catch
    set noundofile
  endtry
endif


"" Visual mode increment/decrement in a row
if v:version > 704 || (v:version == 704 && has('patch754'))
  xnoremap <C-A> <C-A>gv
  xnoremap <C-X> <C-X>gv
endif


"" Visual mode indentation
xnoremap > >gv
xnoremap < <gv


"" ------------------
"" Plugins
"" ------------------

"" Vim plugins
call s:eager_source('plugin.vim')

filetype plugin indent on


"" MatchIt
if exists(':packadd')
  packadd matchit
else
  runtime macros/matchit.vim
endif

augroup vimrc
  autocmd FileType * let b:match_ignorecase = &ignorecase
augroup END


"" Binary editor
command! BinEdit call <SID>binary_edit()

function! s:binary_edit() abort
  if get(b:, 'binary_edit', 0)
    echoerr 'Already in binary-edit mode'
    return
  endif
  silent %!xxd -g 1
  setlocal binary ft=xxd nomodified
  let b:binary_edit = 1
  augroup vimrc
    autocmd BufWritePre <buffer> %!xxd -r
    autocmd BufWritePost <buffer> silent %!xxd -g 1
    autocmd BufWritePost <buffer> setlocal nomod
  augroup END
endfunction


"" Diff from last saved state
command! DiffLast call <SID>difflast_view()

function! s:difflast_view() abort
  let ft = &l:filetype
  vnew
  read #
  0d_
  let &l:filetype = ft
  setlocal bufhidden=wipe buftype=nofile noswapfile
  setlocal nofoldenable nomodified nomodifiable
  nnoremap <silent><buffer> q <C-W>q
  augroup vimrc
    autocmd BufWinLeave <buffer> noautocmd diffoff!
  augroup END
  diffthis
  wincmd p
  diffthis
  redraw!
endfunction


"" Diff option
if !(has('patch-8.1.0360') && &diffopt =~# 'internal')
      \ && &diffexpr ==# '' && executable('diff')
  set diffexpr=DiffExpr()

  function! DiffExpr() abort
    let opt = ''
    if &diffopt =~# 'icase'
      let opt .= '-i '
    endif
    if &diffopt =~# 'iwhite'
      let opt .= '-b '
    endif
    silent! execute '!diff' '--ignore-all-space' '-a' opt v:fname_in v:fname_new '>' v:fname_out
  endfunction
endif


"" Command-line window
nnoremap <Plug>(cmdwin-enter) q:
xnoremap <Plug>(cmdwin-enter) q:
nnoremap <Plug>(cmdwin-enter-norange) q:<C-U>

augroup vimrc
  autocmd CmdwinEnter * call <SID>cmdwin_enter()
augroup END

function! s:cmdwin_enter() abort
  nnoremap <buffer> <CR> <CR>
  xnoremap <buffer> <CR> <CR>

  nnoremap <buffer> q :<C-U>quit<CR>
  nnoremap <buffer> <TAB> :<C-U>quit<CR>

  " Completion.
  inoremap <buffer><expr> <TAB> pumvisible() ? "\<C-N>" : "\<TAB>"
  inoremap <buffer><expr> <CR>  pumvisible() ? "\<C-Y>\<CR>" : "\<CR>"
  inoremap <buffer><expr> <C-H> pumvisible() ? "\<C-Y>\<C-H>" : "\<C-H>"
  inoremap <buffer><expr> <BS>  pumvisible() ? "\<C-Y>\<C-H>" : "\<C-H>"

  startinsert!
endfunction


"" Auto-mkdir with confirm
augroup vimrc
  autocmd BufWritePre * call <SID>auto_mkdir(expand('<afile>:p:h'), v:cmdbang)
augroup END

function! s:auto_mkdir(dir, force) abort
  if !isdirectory(a:dir) && (a:force
        \ || input('Create directory "' . a:dir . '"? ') =~? '^y\%[es]$')
    call mkdir(iconv(a:dir, &encoding, &termencoding), 'p')
  endif
endfunction


"" Do grep and open quickfix window
if has('job')
  let s:grep_job = {}

  function! s:grep_job.out_cb(ch, msg) abort
    caddexpr a:msg
    cwindow
  endfunction

  function! s:grep_job.exit_cb(job, status) abort
    unlet! s:grep_job.handle
  endfunction

  function! s:grep_qfix(...) abort
    if has_key(s:grep_job, 'handle')
      call job_stop(s:grep_job.handle)
      unlet! s:grep_job.handle
    endif
    silent! cexpr ''
    if empty(a:000)
      cclose
      return
    endif
    let cmd = split(&grepprg) + a:000
    if &grepprg =~# '^grep\>' && &grepprg !~# '\$\*'
      let cmd += ['/dev/null']
    endif
    let s:grep_job.handle = job_start(cmd, {
          \ 'in_io': 'null',
          \ 'out_cb': s:grep_job.out_cb,
          \ 'exit_cb': s:grep_job.exit_cb,
          \ })
  endfunction

  command! -nargs=* Grep call <SID>grep_qfix(<f-args>)
else
  if &grepprg =~# '^grep\>' && &grepprg !~# '\$\*'
    let &grepprg .= ' $* /dev/null'
  endif

  function! s:grep_qfix(args) abort
    silent! cexpr ''
    if empty(a:args)
      cclose
      return
    endif
    let save_sp = &shellpipe
    try
      let &shellpipe = '2>/dev/null >'
      execute 'silent!' 'grep!' a:args
    finally
      let &shellpipe = save_sp
    endtry
    redraw!
    cwindow
  endfunction

  command! -nargs=* Grep call <SID>grep_qfix(<q-args>)
endif

function! s:grep_prompt() abort
  try
    let word = input('Pattern: ')
    if word !=# ''
      call s:grep_qfix(word)
    endif
  catch
    " ignore
  endtry
endfunction

nnoremap <silent> ,g :<C-u>Grep \\b<C-R>=escape(expand('<cword>'), '\\.*$^[]')<CR>\\b<CR>
nnoremap <silent> \g :<C-u>call <SID>grep_prompt()<CR>
xnoremap <silent> ,g y:<C-u>Grep <C-R>=escape(@", '\\.*$^[] ')<CR><CR>


"" ------------------
"" Epilogue
"" ------------------

"" Default statusline
if !get(g:, 'plugin_set_statusline', 0)
  set statusline=%!FormStatusLine()
  function! FormStatusLine() abort
    return '%n: %<%f %m%r%h%w%=%c#%oB %l/%LL (%P) %y[%{&ff}][%{'
          \ . (has('multi_byte') && '&fenc' !=# '' ? '&fenc' : '&enc') . '}::U+%04B]'
  endfunction
endif


"" Default tabline
if !get(g:, 'plugin_set_tabline', 0)
  hi TabLine     term=reverse      cterm=reverse      ctermfg=white ctermbg=black
  hi TabLineSel  term=bold         cterm=bold         ctermfg=gray  ctermbg=black
  hi TabLineFill term=bold,reverse cterm=bold,reverse ctermfg=white ctermbg=black

  set tabline=%!FormTabLine()
  function! FormTabLine() abort
    let titles = map(range(1, tabpagenr('$')), 's:tabpage_label(v:val)')
    let tabpages = join(titles, '') . '%#TabLineFill#%T'

    let info = '[' . pathshorten(fnamemodify(getcwd(), ':~')) . ']'

    return tabpages . '%=' . info
  endfunction

  function! s:tabpage_label(tabpagenr) abort
    let title = gettabvar(a:tabpagenr, 'title')
    if title !=# ''
      return title
    endif

    let curtabnr = tabpagenr()
    if a:tabpagenr == curtabnr
      let hl = '%#TabLineSel# '
      let sep = '%<'
    else
      let hl = '%#TabLine#'
      let sep = (a:tabpagenr != curtabnr - 1) ? '|' : ''
      if a:tabpagenr == 1 || a:tabpagenr == curtabnr + 1
        let hl .= ' '
      endif
    endif

    let bufnrs = tabpagebuflist(a:tabpagenr)
    let bufcnt = len(bufnrs)
    if bufcnt == 1
      let bufcnt = ''
    endif

    let mod = len(filter(copy(bufnrs), 'getbufvar(v:val, "&modified")')) ? '+' : ''
    let nomod = (bufcnt . mod) ==# '' ? '' : ' [' . (bufcnt . mod) . ']'

    let curbufnr = bufnrs[tabpagewinnr(a:tabpagenr) - 1]
    let fname = fnamemodify(bufname(curbufnr), ':t')
    if fname ==# ''
      let fname = '*No Name*' 
    endif

    let label = fname . nomod

    return '%' . a:tabpagenr . 'T' . hl . a:tabpagenr . ': ' . label . ' %T' . sep
  endfunction
endif


"" !!! Enable syntax rules !!!
syntax enable


"" Set colorscheme
colorscheme solarized


"" Blank-character highlighting
augroup vimrc
  autocmd BufWinEnter * call <SID>syn_match('User_TabCharacter', '/\t\+/')
  autocmd BufWinEnter * call <SID>syn_match('User_TrailingBlank', '/\s\+$/')
  autocmd BufWinEnter * call <SID>syn_match('User_FullwidthSpace',  '/[\u3000]\+/')
  autocmd ColorScheme * call <SID>hi_define(&bg)
  if exists('##OptionSet')
    autocmd OptionSet background call <SID>hi_define(v:option_new)
  endif
augroup END

function! s:syn_match(synname, pattern) abort
  if !get(b:, 'disable_hl_' . a:synname , 0)
    execute 'syn' 'match' a:synname a:pattern
  endif
endfunction

function! s:hi_define(bg) abort
  if a:bg ==# 'dark'
    " Tab character
    hi User_TabCharacter term=underline ctermbg=DarkGray guibg=DarkGray
    " Trailing blank
    hi User_TrailingBlank term=underline ctermbg=DarkRed guibg=DarkRed
    " Fullwidth space
    hi User_FullwidthSpace term=underline ctermbg=DarkBlue guibg=DarkBlue
  else
    " Tab character
    hi User_TabCharacter term=underline ctermbg=LightGray guibg=LightGray
    " Trailing blank
    hi User_TrailingBlank term=underline ctermbg=Red guibg=Red
    " Fullwidth space
    hi User_FullwidthSpace term=underline ctermbg=Blue guibg=Blue
  endif
endfunction

call s:hi_define(&bg)

"" vim:et:ts=2:sw=2:
