"" lightline.vim

function! s:sid() abort
  if !exists('s:_sid')
    let s:_sid = matchstr(expand('<sfile>'), '<SNR>\zs\d\+\ze_sid$')
  endif
  return s:_sid
endfunction

function! s:funcref(funcname) abort
  return printf('<SNR>%d_%s', s:sid(), a:funcname)
endfunction

let g:lightline = {
      \   'colorscheme': 'solarized',
      \   'active': {
      \     'left': [
      \       [ 'mode', 'paste' ],
      \       [ 'vcsinfo', 'filename' ],
      \     ],
      \     'right': [
      \       [ 'lineinfo', 'charvaluehex' ],
      \       [ 'percent' ],
      \       [ 'fileformat', 'fileencoding', 'filetype' ],
      \     ],
      \   },
      \   'component': {
      \     'lineinfo': '%3l:%2c:%2v',
      \     'charvaluehex': 'U+%04B',
      \   },
      \   'component_function': {
      \     'filename': s:funcref('ll_filename'),
      \     'fileformat': s:funcref('ll_fileformat'),
      \     'filetype': s:funcref('ll_filetype'),
      \     'fileencoding': s:funcref('ll_fileencoding'),
      \     'mode': s:funcref('ll_mode'),
      \     'vcsinfo': s:funcref('ll_vcsinfo'),
      \   },
      \ }

let g:tagbar_status_func = s:funcref('ll_tagbar_status_func')

function! s:ll_modified() abort
  return &ft =~# 'help' ? '' : &modified ? '+' : &modifiable ? '' : '-'
endfunction

function! s:ll_readonly() abort
  return &readonly && &ft !~# 'help' ? 'RO' : ''
endfunction

function! s:ll_filename() abort
  let fname = expand('%:t')
  let readonly = s:ll_readonly()
  let modified = s:ll_modified()
  return (fname !=# '' ? expand('%') : '[No Name]') .
        \ (readonly ==# '' && modified ==# '' ? '' : ' ') .
        \ readonly . modified
endfunction

function! s:ll_currentdir() abort
  return fnamemodify(getcwd(), ':~:h')
endfunction

function! s:ll_fileformat() abort
  return winwidth(0) >= 60 ? &ff : ''
endfunction

function! s:ll_filetype() abort
  return winwidth(0) >= 60 ? (strlen(&ft) ? &ft : '') : ''
endfunction

function! s:ll_fileencoding() abort
  return winwidth(0) >= 60 ? (strlen(&fenc) ? &fenc : &enc) : ''
endfunction

function! s:ll_mode() abort
  return winwidth(0) >= 40 ? lightline#mode() : ''
endfunction

function! s:ll_tagbar_status_func(current, sort, fname, ...) abort
  let g:lightline.fname = a:fname
  return lightline#statusline(0)
endfunction

" components provided by other plugins
if PluginExists('vcs-info.vim')
  function! s:ll_vcsinfo() abort
    let fname = expand('%:t')
    if fname !=# '[Command Line]' && winwidth(0) >= 80
      try
        let [name, branch] = vcs_info#get_branch()
        return name !=# '' ? '⎇ ' . branch . '[' . name . ']' : ''
      catch
        " nop
      endtry
    endif
    return ''
  endfunction
else
  function! s:ll_vcsinfo() abort
    return ''
  endfunction
endif

" declares providing statusline and tabline
let g:plugin_set_statusline = 1
let g:plugin_set_tabline = 1

" cleanup
unlet s:_sid
delfunction s:sid
delfunction s:funcref
