"" agit.vim

augroup vimrc-plugin-agit
  autocmd!
  autocmd FileType agit setlocal nomodeline

  if PluginExists('vim-smooth-scroll')
    function! s:disable_smooth_scroll() abort
      nnoremap <buffer> <C-F> <C-F>
      nnoremap <buffer> <C-B> <C-B>
      nnoremap <buffer> <C-D> <C-D>
      nnoremap <buffer> <C-U> <C-U>
    endfunction
    autocmd FileType agit call <SID>disable_smooth_scroll()
  endif
augroup END
