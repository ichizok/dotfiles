"" caw.vim

nmap <C-_><C-_> <Plug>(caw:hatpos:toggle)
vmap <C-_><C-_> <Plug>(caw:hatpos:toggle)

nmap <C-_>a <Plug>(caw:dollarpos:toggle)
vmap <C-_>a <Plug>(caw:dollarpos:toggle)

nmap <C-_>w <Plug>(caw:wrap:toggle)
vmap <C-_>w <Plug>(caw:wrap:toggle)
