"" lexima.vim

" Don't break undo sequence when cursor moved
" http://qiita.com/yami_beta/items/26995a5c382bd83ac38f
inoremap <C-F> <C-R>=lexima#insmode#leave(1, '<LT>C-G>U<LT>Right>')<CR>
inoremap <C-B> <C-R>=lexima#insmode#leave(1, '<LT>C-G>U<LT>Left>')<CR>
