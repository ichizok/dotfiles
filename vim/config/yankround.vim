"" yankround.vim

nmap p <Plug>(yankround-p)
nmap P <Plug>(yankround-P)
nmap <expr><C-p> yankround#is_active() ? "\<Plug>(yankround-prev)" : "\<C-p>"
nmap <expr><C-n> yankround#is_active() ? "\<Plug>(yankround-next)" : "\<C-n>"
