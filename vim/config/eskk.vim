"" eskk.vim

let g:eskk#directory = expand('~/.config/eskk')
let g:eskk#dictionary = {
      \   'path': g:eskk#directory . '/skk-jisyo',
      \   'sorted': 0,
      \   'encoding': 'utf-8',
      \ }
let g:eskk#large_dictionary = {
      \   'path': g:eskk#directory . '/SKK-JISYO.LL',
      \   'sorted': 1,
      \   'encoding': 'utf-8',
      \ }

let g:eskk#enable_completion = 1

augroup vimrc-plugin-eskk
  autocmd!
  autocmd VimEnter * imap <C-j> <Plug>(eskk:toggle)
  autocmd VimEnter * cmap <C-j> <Plug>(eskk:toggle)
augroup END
