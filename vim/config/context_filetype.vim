"" context_filetype.vim

let g:context_filetype#filetypes = {
      \   'qfix_memo': [
      \     {
      \       'start': '^>|\(\w*\)|$',
      \       'end': '^||<$',
      \       'filetype': '\1'
      \     },
      \   ],
      \ }
