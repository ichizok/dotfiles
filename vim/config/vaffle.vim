"" vaffle.vim

" disable netrw
let g:loaded_netrwPlugin = 1

augroup vimrc-plugin-vaffle
  autocmd!
  autocmd FileType vaffle nmap <buffer><silent> c <Plug>(vaffle-chdir-here)
augroup END
