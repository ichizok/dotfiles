"" ctrlp.vim

let g:ctrlp_map = '\f'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_regexp = 1
let g:ctrlp_working_path_mode = ''

if executable('fd')
  let g:ctrlp_user_command = printf('"%s" -a -t f . %%s', exepath('fd'))
elseif executable('files')
  let g:ctrlp_user_command = printf('"%s" -a %%s', exepath('files'))
endif

nnoremap <silent> \b :<C-U>CtrlPBuffer<CR>
nnoremap <silent> \c :<C-U>CtrlPChange<CR>
nnoremap <silent> \d :<C-U>CtrlPDir<CR>
nnoremap <silent> \l :<C-U>CtrlPLine<CR>
nnoremap <silent> \m :<C-U>CtrlPMRU<CR>

if PluginExists('yankround.vim')
  nnoremap <silent> \p :<C-U>CtrlPYankRound<CR>
endif
