"" Plugin manager

" Must define 5 functions: 
"   - PluginExists({name})
"   - PluginEnable({name})
"   - PluginPath({name})
"   - PluginNames()
"   - PluginConfigure()
let s:plugin_vim = expand('<sfile>:p:h') . '/dein.vim'
" let s:plugin_vim = expand('<sfile>:p:h') . '/minpac.vim'

source `=s:plugin_vim`

call PluginConfigure()

function! PluginComplete(arglead, cmdline, cursorpos) abort
  return filter(PluginNames(), 'v:val =~# "^" . a:arglead')
endfunction

command -nargs=1 -complete=customlist,PluginComplete PluginEnable call PluginEnable(<f-args>)

" Edit plugin.vim
execute 'nnoremap <silent> <Leader>eb :<C-U>edit ' . s:plugin_vim . '<CR>'

unlet s:plugin_vim
