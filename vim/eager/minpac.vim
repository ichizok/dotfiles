"" Vim plugins

if !has('packages')
  echoerr 'requires package feature'
  finish
endif

let $VIMPLUGIN = $VIMHOME . '/pack/minpac'

let s:plugs = {}
let s:plugs_ft = {}

function! s:minpac_add(plugname, ...) abort
  let l:name = fnamemodify(a:plugname, ':t')
  let l:opt = get(a:000, 0, {})

  if has_key(l:opt, 'ft')
    for l:ft in l:opt['ft']
      let s:plugs_ft[l:ft] = get(s:plugs_ft, l:ft, []) + [l:name]
    endfor
    call remove(l:opt, 'ft')
    let l:opt['type'] = 'opt'
  endif

  let s:plugs[l:name] = {'repo': a:plugname, 'opt': l:opt}
endfunction

call s:minpac_add('k-takata/minpac', {'type': 'opt'})

call s:minpac_add('andymass/vim-matchup')
call s:minpac_add('cocopon/vaffle.vim')
call s:minpac_add('cohama/lexima.vim')
call s:minpac_add('ctrlpvim/ctrlp.vim')
call s:minpac_add('easymotion/vim-easymotion')
call s:minpac_add('haya14busa/vim-asterisk')
call s:minpac_add('hotwatermorning/auto-git-diff')
call s:minpac_add('ichizok/vcs-info.vim')
call s:minpac_add('ichizok/vim-smooth-scroll')
call s:minpac_add('itchyny/lightline.vim')
call s:minpac_add('itchyny/vim-qfedit')
call s:minpac_add('junegunn/vim-easy-align')
call s:minpac_add('lambdalisue/vim-diffa')
call s:minpac_add('lambdalisue/vim-gina')
call s:minpac_add('LeafCage/yankround.vim')
call s:minpac_add('machakann/vim-sandwich')
call s:minpac_add('mbbill/undotree')
call s:minpac_add('rhysd/clever-f.vim')
call s:minpac_add('thinca/vim-localrc')
call s:minpac_add('thinca/vim-prettyprint')
call s:minpac_add('thinca/vim-qfreplace')
call s:minpac_add('thinca/vim-quickrun')
call s:minpac_add('thinca/vim-ref')
call s:minpac_add('tyru/caw.vim')

delfunction s:minpac_add


" Load filetype plugins on `autocmd FileType`
function! s:enable_plugs_ft(filetype) abort
  for l:plug in s:plugs_ft[a:filetype]
    packadd `=l:plug`
  endfor
  execute 'doautocmd <nomodeline> FileType' a:filetype
endfunction

augroup vimrc-filetype-plugins
  autocmd!
  if !empty(s:plugs_ft)
    execute 'autocmd FileType'
          \ join(keys(s:plugs_ft), ',')
          \ '++once call s:enable_plugs_ft(expand("<amatch>"))'
  endif
augroup END


" Define user commands for updating/cleaning the plugins.
" Each of them calls PackInit() to load minpac and register
" the information of plugins, then performs the task.
function! PackInit() abort
  packadd minpac

  call minpac#init()

  for l:plug in values(s:plugs)
    call minpac#add(l:plug.repo, l:plug.opt)
  endfor
endfunction

command! PackUpdate call PackInit() | call minpac#update('', {'do': 'call minpac#status()'})
command! PackClean  call PackInit() | call minpac#clean()
command! PackStatus call PackInit() | call minpac#status()


"" Configure plugins
function! PluginExists(name) abort
  return has_key(s:plugs, a:name)
endfunction

function! PluginEnable(name) abort
  packadd `=a:name`
endfunction

function! PluginPath(name) abort
  if has_key(s:plugs, a:name)
    let l:type = get(s:plugs[a:name].opt, 'type', 'start')
    return $VIMPLUGIN . '/' . l:type . '/' . a:name
  endif
  return ''
endfunction

function! PluginNames() abort
  return sort(keys(s:plugs))
endfunction

function! PluginConfigure() abort
  let l:plugs = PluginNames()
  for l:conf in glob($VIMHOME . '/config/*', 0, 1)
    if index(l:plugs, fnamemodify(l:conf, ':t')) >= 0
      source `=l:conf`
    endif
  endfor
endfunction
