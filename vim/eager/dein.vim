"" Vim plugins

if v:version < 704
  echoerr 'requires vim 7.4 or later'
  finish
endif

let $VIMPLUGIN = $HOME . '/.cache/dein'

if !isdirectory($VIMPLUGIN . '/repos/github.com/Shougo/dein.vim')
  echoerr 'dein.vim isn''t installed'
  finish
endif

set runtimepath^=$VIMPLUGIN/repos/github.com/Shougo/dein.vim

if dein#load_state($VIMPLUGIN)
  call dein#begin($VIMPLUGIN, [expand('<sfile>')])

  call dein#add('Shougo/dein.vim')

  call dein#add('andymass/vim-matchup')
  call dein#add('cocopon/vaffle.vim')
  call dein#add('cohama/agit.vim', {
        \   'on_cmd': ['Agit', 'AgitFile'],
        \ })
  call dein#add('cohama/lexima.vim', {
        \   'on_i': 1,
        \ })
  call dein#add('ctrlpvim/ctrlp.vim')
  call dein#add('easymotion/vim-easymotion', {
        \   'on_map': ['<Plug>(easymotion'],
        \ })
  call dein#add('haya14busa/vim-asterisk')
  call dein#add('hotwatermorning/auto-git-diff')
  call dein#add('hrsh7th/vim-vsnip')
  call dein#add('hrsh7th/vim-vsnip-integ')
  call dein#add('ichizok/vcs-info.vim')
  call dein#add('ichizok/vim-smooth-scroll')
  call dein#add('itchyny/lightline.vim')
  call dein#add('itchyny/vim-qfedit')
  call dein#add('junegunn/vim-easy-align', {
        \   'on_map': ['<Plug>(EasyAlign)'],
        \ })
  call dein#add('kana/vim-operator-user')
  call dein#add('kana/vim-submode')
  call dein#add('kana/vim-textobj-user')
  call dein#add('lambdalisue/vim-diffa')
  call dein#add('lambdalisue/vim-gina')
  call dein#add('LeafCage/yankround.vim')
  call dein#add('machakann/vim-sandwich')
  call dein#add('mattn/vim-lsp-settings')
  call dein#add('mbbill/undotree', {
        \   'on_cmd': ['Undotree'],
        \ })
  call dein#add('osyo-manga/vim-precious', {
        \   'lazy': 1,
        \ })
  call dein#add('prabirshrestha/asyncomplete.vim')
  call dein#add('prabirshrestha/asyncomplete-lsp.vim')
  call dein#add('prabirshrestha/vim-lsp')
  call dein#add('rhysd/clever-f.vim')
  call dein#add('sheerun/vim-polyglot')
  call dein#add('Shougo/context_filetype.vim')
  call dein#add('Shougo/vimproc.vim', {
        \   'build': 'make',
        \ })
  call dein#add('thinca/vim-localrc')
  call dein#add('thinca/vim-prettyprint')
  call dein#add('thinca/vim-qfreplace')
  call dein#add('thinca/vim-quickrun')
  call dein#add('thinca/vim-ref')
  call dein#add('tyru/caw.vim')

  call dein#end()
  call dein#save_state()
endif


"" Installation check
"if !dein#check_install()
"  dein#install()
"endif


"" Configure plugins
function! PluginExists(name) abort
  return dein#tap(a:name)
endfunction

function! PluginEnable(name) abort
  if !dein#is_sourced(a:name)
    call dein#source(a:name)
  endif
endfunction

function! PluginPath(name) abort
  return get(dein#get(a:name), 'path', '')
endfunction

function! PluginNames() abort
  return sort(keys(dein#get()))
endfunction

function! PluginConfigure() abort
  for l:conf in glob($VIMHOME . '/config/*', 0, 1)
    if dein#tap(fnamemodify(l:conf, ':t'))
      source `=l:conf`
    endif
  endfor
endfunction
