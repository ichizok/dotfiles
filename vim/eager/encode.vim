scriptencoding utf-8

"=============================================================================
"   Description: UTF-8化と文字コード自動認識設定
"                http://www.kawaz.jp/pukiwiki/?vimを改変
"        Author: fuenor <fuenor@gmail.com>
"                http://sites.google.com/site/fudist/Home/vim-nihongo-ban/vim-utf8
" Last Modified: 2011-03-06 13:03
"       Version: 1.18
"=============================================================================

if exists("g:vimrc_set_encoding")
  finish
endif
let g:vimrc_set_encoding = 1

" Window用設定
if has('win32') || has('win64')

  " vimfiles (.vim) に特定のファイルが存在する場合は以下のように設定される。
  " デフォルトはUTF-8
  "--------------------------------------------------------------
  "| ファイル名    |                                            |
  "|---------------|---------------------------------------------
  "| cp932         | 内部エンコーディングをcp932に設定。        |
  "| cp932+        | 内部エンコーディングがutf-8の時、          |
  "|               | 文字コードの自動認識をcp932優先に設定。    |
  "| utf-8-console | Windowsの非GUI(コンソール版)をUTF-8に設定。|
  "--------------------------------------------------------------
  if !has('gui_running')
    set termencoding=cp932
    " Windowsの非GUI版は内部エンコーディングをUTF-8にすると漢字入力出来ないので、
    " 内部エンコーディングをcp932にする
    if filereadable($VIMHOME . '/utf-8-console')
      set encoding=utf-8
    else
      " 強制的にcp932に変更
      set encoding=cp932
    endif
  elseif filereadable($VIMHOME . '/cp932')
    set encoding=cp932
  endif

  "----------------------------------------
  " 内部エンコーディングを cp932以外にしていて、
  " 環境変数に日本語を含む値を設定したい場合に使用する
  " Let $HOGE = $USERPROFILE.'/ほげ'
  "----------------------------------------
  if has('iconv')
    command! -nargs=+ Let exec 'let' iconv(<q-args>, &enc, 'cp932')
  else
    command! -nargs=+ Let let <args>
  endif
endif

" fileencodingsのデフォルト設定
if &encoding == 'utf-8'
  set fileencodings=ucs-bom,utf-8,default,latin1
else
  set fileencodings=ucs-bom
endif

if has('iconv')
  if &encoding == 'utf-8'
    set fileencodings=ucs-bom,utf-8,iso-2022-jp,euc-jp,cp932,utf-16le,utf-16,default,latin1
  else
    set fileencodings=ucs-bom,utf-8,ucs-2le,ucs-2,iso-2022-jp,euc-jp,cp932
  endif

  " iconvがeucJP-msに対応しているかをチェック
  if iconv("\x87\x64\x87\x6a", 'cp932', 'eucjp-ms') ==# "\xad\xc5\xad\xcb"
    let &fileencodings = substitute(&fileencodings, 'iso-2022-jp', 'iso-2022-jp-3', 'g')
    let &fileencodings = substitute(&fileencodings, 'euc-jp', 'euc-jp-ms', 'g')
  " iconvがJISX0213に対応しているかをチェック
  elseif iconv("\x87\x64\x87\x6a", 'cp932', 'euc-jisx0213') ==# "\xad\xc5\xad\xcb"
    let &fileencodings = substitute(&fileencodings, 'iso-2022-jp', 'iso-2022-jp-3', 'g')
    let &fileencodings = substitute(&fileencodings, 'euc-jp', 'euc-jisx0213', 'g')
  endif
endif

" 改行コードの自動認識
set fileformats=unix,dos,mac

if exists('#OptionSet')
  augroup vimrc-encode
    autocmd!
    autocmd OptionSet ambiwidth let $RUNEWIDTH_EASTASIAN = v:option_new ==# 'double'
  augroup END
endif

if &termencoding ==# ''
  let &termencoding = &encoding
endif

"" vim:set et ts=2 sw=2:
