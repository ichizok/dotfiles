"" Zsh

if exists("b:did_ftplugin_after")
  finish
endif
let b:did_ftplugin_after = 1

if expand('%:p') =~# '/\.\?zsh_history'
  setlocal nomodeline nobuflisted bufhidden=wipe
endif
