"" Vim Help

if exists("b:did_ftplugin_after")
  finish
endif
let b:did_ftplugin_after = 1

nnoremap <buffer> q <C-w>c

let b:disable_hl_User_TabCharacter = 1
let b:disable_hl_User_TrailingBlank = 1
let b:disable_hl_User_FullwidthSpace = 1
