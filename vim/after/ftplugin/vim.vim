"" Vim

if exists("b:did_ftplugin_after")
  finish
endif
let b:did_ftplugin_after = 1

setlocal et sw=2
