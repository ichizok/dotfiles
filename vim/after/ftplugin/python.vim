"" Python

if exists("b:did_ftplugin_after")
  finish
endif
let b:did_ftplugin_after = 1

setlocal et ts=8 sw=4 sts=4
setlocal smartindent
setlocal cinwords=if,elif,else,for,while,try,except,finally,def,class
