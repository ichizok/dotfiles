"" QuickFix window

if exists("b:did_ftplugin_after")
  finish
endif
let b:did_ftplugin_after = 1

if has('patch-7.4.2215')
      \ && get(getwininfo(win_getid())[0], 'loclist', 0)
  nnoremap <buffer><silent> q :<C-U>lclose<CR>
  nnoremap <buffer><silent> ge :<C-U>lexpr ''<CR>
  nnoremap <buffer><silent> gq :<C-U>lexpr ''\|lclose<CR>
else
  nnoremap <buffer><silent> q :<C-U>cclose<CR>
  nnoremap <buffer><silent> ge :<C-U>cexpr ''<CR>
  nnoremap <buffer><silent> gq :<C-U>cexpr ''\|cclose<CR>
endif
