if [[ $- != *i* ]] ; then
    # Shell is non-interactive.  Be done now!
    return
fi

if [[ ${BASH_VERSINFO[0]} -ge 4 ]]; then
    shopt -s autocd
    shopt -s globstar
fi
shopt -s nullglob
shopt -u histappend

if [[ -t 0 ]]; then
    stty werase undef
fi

export EDITOR=vim
export LC_ALL=en_US.UTF-8
export LANG=C
export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=10000
export HISTFILESIZE=${HISTSIZE}

# Merge command histories of each terminal
_histmerge() {
    history -a
    #history -c
    #history -r
}

# Prepare prompt
_prompt_command() {
    _histmerge
    _ps1_jobs=$(jobs -p)
}

export PS1='\e[$([[ $? -eq 0 ]] && echo 32 || echo 31);1m[\u@\h]\e[0m(\D{%Y-%m-%d %H:%M:%S}) \e[33;1m\w\e[0m\n${_ps1_jobs:+[\j]}\$ '
export PROMPT_COMMAND=_prompt_command

# Load additional configs
if [[ -d ~/.bash ]]; then
    for bashrc in ~/.bash/**/*.bash; do
        source "${bashrc}"
    done
    unset bashrc
fi
