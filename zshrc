## start profiling
#zmodload zsh/zprof
#zprof

## set options
setopt always_last_prompt
setopt auto_cd
setopt auto_menu
setopt auto_param_keys
setopt auto_pushd
setopt auto_remove_slash
setopt cdable_vars
setopt complete_aliases
setopt correct
setopt extended_glob
setopt extended_history
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt inc_append_history
setopt list_types
setopt long_list_jobs
setopt magic_equal_subst
setopt no_beep
setopt prompt_subst
setopt pushd_ignore_dups
setopt pushd_minus
setopt rm_star_silent
setopt sh_word_split
setopt sun_keyboard_hack

## keybind
bindkey -e
bindkey "^[[3~" delete-char
bindkey "^X^J" push-line
bindkey "^X^H" run-help

## historical backward/forward search with linehead string binded to ^P/^N
autoload -Uz history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end

## command history configuration
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000

## exclude / (slash) from WORDCHARS
WORDCHARS=${WORDCHARS:s#/#}

## set paths dedup (NOTE: 'path' is in zshenv)
typeset -U cdpath fpath infopath manpath

## tie new paths
typeset -T INFOPATH infopath

## set zsh-function paths
fpath=(~/.zsh/functions{,/**/*(N-/)} "${fpath[@]}")
fpath=("${^fpath[@]}"(N-/^W))

## enable completion
autoload -Uz compinit
compinit -C

## enable colors
autoload -Uz colors
colors

## enable command-line edit
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey "^Xe" edit-command-line

## set prompt
MY_PROMPT_0="%U%B%F{red}%n@%m%f%b%u [%D{%F %T}] [%B%(?.%F{green}.%F{red})%?%f%b] [%F{yellow}%~%f]"
MY_PROMPT_1="%(1j,[%j],)%#"
PROMPT="\
${MY_PROMPT_0}
${MY_PROMPT_1} "

## autoload user functions ('' is for avoiding empty argument)
autoload -Uz ~/.zsh/functions/**/*(.Nr:t) ''

## load additional configs (if `zreloadrc' exists)
if command -v zreloadrc &>/dev/null; then
    zreloadrc
fi

## stop profiling and show results
#zprof
