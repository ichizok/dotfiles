"" Reset augroup gvimrc
augroup gvimrc
  autocmd!
augroup END

"colorscheme default

"set columns=80
"set lines=24

set formatoptions-=m
"set guifont=Source\ Code\ Pro\ Light:h13
set guioptions-=T
"set imdisable
set showtabline=2
"set visualbell t_vb=

if has('gui_macvim')
  set antialias
  "set fuoptions=maxvert,maxhorz
  "set transparency=10
endif

augroup gvimrc
  if has('gui_macvim')
    "" Full-screen
    "autocmd GUIEnter * set fullscreen
  endif
augroup END

function! s:uniq(list) abort
  let newlist = []
  let seen = {}
  for i in a:list
    if !has_key(seen, i)
      let newlist += [i]
      let seen[i] = 0
    endif
  endfor
  return newlist
endfunction

function! s:fix_path(paths) abort
  let paths = filter(map(s:uniq(a:paths),
        \ 'fnamemodify(v:val, ":p")'), 'isdirectory(v:val)')
  let $PATH = join(paths, ':')
endfunction

"" Set executable paths
"call s:fix_path(split($PATH, ':'))

"" Cleanup
delfunction s:uniq
delfunction s:fix_path
