## modules
autoload -Uz zargs

## language
export LANG=en_US.UTF-8

## execute-path
typeset -U path
path=("${^path[@]}"(N-/^W))
