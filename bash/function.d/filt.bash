if command -v fzf &>/dev/null; then
    # fcd (filter-cd)
    function fcd() {
        local dir_tmp=
        local dir_path=${1:-.}
	local dot_line_show="[show dot]"
	local dot_line_hide="[hide dot]"
	local dot_line=${dot_line_show}
        while true; do
            dir_path=$(\cd "${dir_path/\/\///}"; pwd)
            if ! [[ "${dir_path}" = / ]]; then
                dir_path=${dir_path%/}
            fi
            if [[ "${dir_path}" =~ ^${PWD} ]]; then
                dir_path=.${dir_path#${PWD}}
            fi
            dir_tmp=$(
                shopt -s nullglob
                dir_list=("${dir_path}"/*/)
                if [[ "${dot_line}" = "${dot_line_hide}" ]]; then
                    dir_list=("${dir_path}"/.*/ "${dir_list[@]}")
                fi
                {
                    echo "[cd ${dir_path}]"
                    if ! [[ "${dir_path}" = / ]]; then
                        echo ..
                    fi
                    for d in "${dir_list[@]%/}"; do
                        if ! [[ "${d}" =~ /\.\.?$ ]]; then
                            echo "${d##*/}"
                        fi
                    done
                    echo "${dot_line}"
                } | env FZF_DEFAULT_OPTS= fzf --exact --layout=reverse --info=inline)
	    if [[ $? -ne 0 ]]; then
                return
            elif [[ "${dir_tmp}" = "[cd ${dir_path}]" ]]; then
                \cd "${dir_path}"
                return
            elif [[ "${dir_tmp}" = "${dot_line}" ]]; then
                if [[ "${dot_line}" = "${dot_line_show}" ]]; then
                    dot_line=${dot_line_hide}
                else
                    dot_line=${dot_line_show}
                fi 
            else
                dir_path=${dir_path}/${dir_tmp}
            fi
        done
    }

    # select command from history
    function filter-command-history() {
        local buffer=$(\fc -l -r -n 1 | sed 's/^\s*//' | env FZF_DEFAULT_OPTS= \
            fzf --exact --no-sort --layout=reverse --info=inline --query="${READLINE_LINE}")
	READLINE_LINE=${buffer}
	READLINE_POINT=${#buffer}
    }

    bind -x '"\C-r": filter-command-history'
fi
