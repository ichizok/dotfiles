alias vim='vim -p'
alias vi=vim
alias l=ls
alias la='ls -A'
alias ll='ls -Al'
